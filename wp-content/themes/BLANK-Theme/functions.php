<?php
	
	// Add RSS links to <head> section
	automatic_feed_links();
	
	//Use this GLOBAL to include a FILE ~> TEMPLATEPATH

    // Load jQuery
	if ( !is_admin() ) {
	   wp_deregister_script('jquery');
	   wp_register_script('jquery', ("http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"), false);
	   wp_enqueue_script('jquery');
	}
	
	// Clean up the <head>
	function removeHeadLinks() {
    	remove_action('wp_head', 'rsd_link');
    	remove_action('wp_head', 'wlwmanifest_link');
    }
    add_action('init', 'removeHeadLinks');
    remove_action('wp_head', 'wp_generator');
    
	// Declare sidebar widget zone
    if (function_exists('register_sidebar')) {
    	register_sidebar(array(
    		'name' => 'Sidebar Widgets',
    		'id'   => 'sidebar-widgets',
    		'description'   => 'These are widgets for the sidebar.',
    		'before_widget' => '<div id="%1$s" class="widget %2$s">',
    		'after_widget'  => '</div>',
    		'before_title'  => '<h2>',
    		'after_title'   => '</h2>'
    	));
    }

    if(function_exists('register_nav_menus')){
        register_nav_menus(
            array(
                'main_nav' => 'Main Nav'
            )
        );
    }

    if(!function_exists('includeFile')){
        function includeFile($uri = '')
        {
            $file_uri = get_stylesheet_directory_uri();
            return $file_uri . '/' . $uri;
        }
    }
/*
    add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1);
    add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1);
    add_filter('page_css_class', 'my_css_attributes_filter', 100, 1);
    function my_css_attributes_filter($var) {
        return is_array($var) ? array() : '';
    }
*/
    register_sidebar(array(
        'name' => __( 'Contact Us Widget' ),
        'id' => 'contact-sidebar',
        'description' => __( 'Sidebar Widgets that will be shown on the Contact Us Tab only.' ),
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));
    register_sidebar(array(
        'name' => __( 'Footer Widget' ),
        'id' => 'footer-widget',
        'description' => __( 'Footer Widgets' ),
        'before_widget' => '<div id="%1$s" class="footer-logo %2$s">',
        'after_widget' => '</div>',
        'before_title' => '',
        'after_title' => ''
    ));
    register_sidebar(array(
        'name' => __( 'Copyright Widget' ),
        'id' => 'copyright-widget',
        'description' => __( 'Copyright Widgets' ),
        'before_widget' => '<div id="%1$s" class="copyright %2$s">',
        'after_widget' => '</div>',
        'before_title' => '',
        'after_title' => ''
    ));

    add_theme_support( 'post-thumbnails' ); 

?>