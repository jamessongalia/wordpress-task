<?php if($post->post_name != 'home'): ?>
<div class="sidebar">
<div class="sidebar-con">
<?php if ( has_post_thumbnail() ): ?>
	<h3>Featured Image</h3>
	<?php the_post_thumbnail('thumbnail'); ?>
<?php endif; ?>
<?php if($post->post_name == 'contact-us'): ?>
    <?php dynamic_sidebar( 'contact-sidebar' ); ?>
<?php else: ?>
    <?php dynamic_sidebar( 'sidebar-widgets' ); ?>
    <?php //wp_list_categories(array('hide_empty'=>0,'show_count'=>1)); ?>
<?php endif; ?>
</div>
</div>
<?php endif; ?>
<div class="clear"></div>