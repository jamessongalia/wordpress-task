<?php get_header(); ?>
    <div class="content">
        <div class="left-con">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <div>
            <h1><?php the_title(); ?></h1>
            <p>
            <em>Posted on:</em> <?php the_time('F jS, Y') ?>
            <em>by:</em> <?php the_author(); ?>
            <?php the_content(); ?>
            </p>
            </div>
			<?php comments_template(); ?>
		<?php endwhile; endif; ?>
        </div>
    <?php get_sidebar(); ?>
   	</div>
<?php get_footer(); ?>