<?php get_header(); ?>
    <div class="content">
    <?php if($post->post_name == 'home'): ?>
        <?php if(have_posts()): while(have_posts()): the_post(); ?>
        <?php the_content(); ?>
        <?php endwhile; ?>
        <?php else: ?>
        <h2>No Post</h2>
        <?php endif; ?>
    <?php elseif($post->post_name == 'blog'): ?>
        <div class="left-con">
            <?php
                $post_per_page = 5;
                $current = max( 1, get_query_var('paged') );
                $big = 9999999999; // need an unlikely integer
                $total = ceil(wp_count_posts()->publish / $post_per_page);
                if($current > $total){ $current = $total; }
                $posts = get_posts(array('posts_per_page'=>$post_per_page,'offset'=>(($current-1) * $post_per_page)));
                foreach($posts as $post): setup_postdata($post);
            ?>
            <div>
            <h1><?php the_title(); ?></h1>
            <p>
            <em>Posted on:</em> <?php the_time('F jS, Y') ?>
            <em>by:</em> <?php the_author(); ?>
            <?php the_excerpt(); ?>
            <a href="<?php the_permalink(); ?>">Read more..</a>
            </p>
            </div>
            <?php endforeach; ?>
            <!-- Add the pagination functions here. -->
            <p>Page: 
            <?php
            echo paginate_links( array(
                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                'format' => '?paged=%#%',
                'current' => max( 1, get_query_var('paged') ),
                'total' => $total
            ) );
            ?></p>
        </div>
    <?php else: ?>
    	<div class="left-con">
    		<?php if(have_posts()): while(have_posts()): the_post(); ?>
    		<h1><?php the_title(); ?></h1>
    		<?php the_content(); ?>
    		<?php endwhile; ?>
    		<?php else: ?>
    		<h2>No Post</h2>
    		<?php endif; ?>
    		<?php //echo $post->post_content; ?>
       	</div>
    <?php endif; ?>
    <?php get_sidebar(); ?>
   	</div>
<?php get_footer(); ?>