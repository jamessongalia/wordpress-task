    <div class="footer">
    	<div class="navbar-footer">
        	<div class="navbar-con">
            <?php
            $defaults = array(
                'theme_location'  => '',
                'menu'            => 'Main Nav',
                'container'       => 'li',
                'container_class' => '',
                'container_id'    => '',
                'menu_class'      => '',
                'menu_id'         => 'menu',
                'echo'            => true,
                'fallback_cb'     => 'wp_page_menu',
                'before'          => '',
                'after'           => '',
                'link_before'     => '',
                'link_after'      => '',
                'items_wrap'      => '<ul id="%1$s"><li><a href="'.get_home_url().'/index.php">Home</a></li>%3$s</ul>',
                'depth'           => 0,
                'walker'          => ''
            );
            wp_nav_menu( $defaults );
            ?>
            </div>
      </div>
        <div class="clear"></div>
        <?php dynamic_sidebar('footer-widget'); ?>
        <?php dynamic_sidebar('copyright-widget'); ?>
    </div>
</div>
<?php wp_footer();?>
</body>
</html>