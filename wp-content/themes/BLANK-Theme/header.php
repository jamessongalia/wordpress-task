<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>MDS Boring & Drilling</title>
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans">
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" type="text/css" href="<?php echo includeFile('style.css'); ?>"/>
<?php wp_head();?>
</head>

<body>
<div class="wrapper">

	<div class="header">
    	<div class="header-margin"></div>
        <div class="header-con">
            <?php lm_display_logo(); ?>
            <!--
        	<div class="logo">
            	<span><h1>Slick & Dry Boring</h1></span>
        		<span><h1><a href="#" title="" rel=""><img src="<?php echo includeFile('images/MDS-boring&drilling.png'); ?>" width="287" height="101" /></a></h1></span>
               <span> <h1>Directional Drilling</h1></span>
            </div>
            -->
        </div>
        <div class="header-margin"></div>
        <div class="navbar">
        	<div class="navbar-con">
        		<?php
        		$defaults = array(
					'theme_location'  => '',
					'menu'            => 'Main Nav',
					'container'       => 'li',
					'container_class' => '',
					'container_id'    => '',
					'menu_class'      => '',
					'menu_id'         => 'menu',
					'echo'            => true,
					'fallback_cb'     => 'wp_page_menu',
					'before'          => '',
					'after'           => '',
					'link_before'     => '',
					'link_after'      => '',
					'items_wrap'      => '<ul id="%1$s"><li><a href="'.get_home_url().'/index.php">Home</a></li>%3$s</ul>',
					'depth'           => 0,
					'walker'          => ''
				);
				wp_nav_menu( $defaults );
        		?>
          	</div>
        </div>
    </div>
     <div class="clear"></div>