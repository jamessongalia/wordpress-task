<?php get_header(); ?>

	<div class="content">
		<div class="slideshow">
	    	
	    </div>
	    <div class="right-con">
	    <?php
	    	$page = get_page_by_title('Home');
	    	if($page){
	    		echo $page->post_content;
	    	}else{
	    		echo '<h1>No Data</h1>';
	    	}
	    ?>
	    </div>
	    <div class="clear"></div>
	</div>

<?php get_footer(); ?>